
# Example Initialization of ImageSequencer #

## DOM Structure ##

```
  <section id="sequence" class="mod-image-sequencer " data-fps="5" data-height="58em" data-slider="true">
        <div class="mod-img-sequence-container">
            <img src="images/img_1.png" />
            <img src="images/img_2.png" />
            <img src="images/img_3.png" />
            <img src="images/img_4.png" />
            <img src="images/img_5.png" />
            <img src="images/img_6.png" />
            <img src="images/img_7.png" />
            <img src="images/img_8.png" />
            <img src="images/img_9.png" />
            <img src="images/img_10.png" />
            <img src="images/img_11.png" />
            <img src="images/img_12.png" />
            <img src="images/img_13.png" />
            <img src="images/img_14.png" />
            <img src="images/img_15.png" />
            <img src="images/img_16.png" />
            <img src="images/img_17.png" />
            <img src="images/img_18.png" />
        </div>
          
        <div class='controls'>
            <div class="slider"></div>
        </div>
    </section>
```

# Single Sequence #

```
var elements = $('.mod-image-sequencer');
var imageSequencer = new ImageSequencer(
    elements.attr('id'),
    elements.find('.mod-img-sequence-container'),
    elements.data('fps'),
    elements.data('height'),
    elements.data('slider')
);                                             

```

# Multiple Sequences #

```
$(function() {
    var imageSequencers = [];
    if ($('.mod-image-sequencer').length > 0) {
        for (var i = 0; i < $('.mod-image-sequencer').length; i++) {
            var temp = $('.mod-image-sequencer').eq(i);
            if (temp.find('.mod-img-sequence-container').length > 1) {
                for (var j = 0; j < temp.find('.mod-img-sequence-container').length; j++) {
                    imageSequencers.push(new ImageSequencer(
                        temp.attr('id'),
                        temp.find('.mod-img-sequence-container').eq(j),
                        temp.data('fps'),
                        temp.data('height'),
                        temp.data('slider')
                    ));
                }
            } else {
                imageSequencers.push(new ImageSequencer(
                    temp.attr('id'),
                    temp.find('.mod-img-sequence-container'),
                    temp.data('fps'),
                    temp.data('height'),
                    temp.data('slider')
                ));
            }
        }
    }
});
```

##### As for the CSS #####
There is an included CSS file that'll get you started but it is advised to do your own styling to better fit your needs